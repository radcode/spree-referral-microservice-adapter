require 'spree_referral_microservice_adapter/configuration'
require 'spree_core'
require 'spree_extension'
require 'spree_referral_microservice_adapter/engine'
require 'spree_referral_microservice_adapter/version'

module Spree
  module ReferralMicroserviceAdapter
    class << self
      attr_accessor :configuration
    end

    def self.configuration
      @configuration ||= Configuration.new
    end

    def self.reset
      @configuration = Configuration.new
    end

    def self.configure
      yield(configuration)
    end
  end
end

module Spree
  module ReferralMicroserviceAdapter
    class Configuration
      attr_accessor :path, :connection_options

      def initialize
        @path = ENV["REFERRAL_MICROSERVICE_API_ENDPOINT"] || 'http://localhost:4000/api'
        @connection_options = lambda do |conn|
          conn.request :json
          conn.use ::FaradayMiddleware::FollowRedirects
          conn.response :logger, ::Logger.new(STDOUT), bodies: true
          conn.adapter Faraday.default_adapter
        end
      end
    end
  end
end
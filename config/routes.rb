Spree::Core::Engine.add_routes do
  namespace :api, defaults: { format: 'json' } do
    namespace :v1 do
      resources :referral_codes, only: [:show]
      get 'referral_codes/:id/owner_name', to: 'referral_codes#owner_name'

      resources :coupons
      resources :users do
        member do
          post 'refer' => 'users#handle_referral_request'
          get 'coupons' => 'users#list_coupons'
          get 'credits' => 'users#list_credits'
          get 'refer_stats' => 'users#list_refer_stats'
        end
      end
    end
  end
end

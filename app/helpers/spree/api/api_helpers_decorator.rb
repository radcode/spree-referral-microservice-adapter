Spree::Api::ApiHelpers.class_eval do
  # here you can extend range of parameters that get accepted by API controllers
  user_attributes << :firstname << :lastname << :phone << :birthday << :referral_code
end

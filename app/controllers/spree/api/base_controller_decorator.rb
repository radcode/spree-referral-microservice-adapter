Spree::Api::BaseController.class_eval do
  # helper function - trying to run default `self.render` of a controller results in an error
  # Spree controllers' config for V1 api doesn't explicitly set the version so I do that here
  # this way the renderer doesn't fail to find a template for the view due to the version number missing
  def render_anywhere(view, options = {})
    service = VersionCake::VersionContextService.new(VersionCake.config)
    empty_request = ActionDispatch::Request.new({})
    self.request = ActionDispatch::Request.new({'versioncake.context' => service.create_context_from_request(empty_request), "rack.input" => -> {},   "content_type" => "js"})
    set_version(1)
    render_to_string(view, options)
  end
end

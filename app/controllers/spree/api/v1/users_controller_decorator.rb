Spree::Api::V1::UsersController.class_eval do
  def handle_referral_request
    response = connection.post "users/#{user.id}/refer", params.to_json
    render inline: response.body.html_safe, layout: false, status: response.status
  end

  def list_coupons
    render json: { coupons: user.coupons }.to_json
  end

  def list_credits
    render json: { credits: user.credits }.to_json
  end

  def list_refer_stats
    render json: { refer_stats: user.refer_stats }.to_json
  end

  private

  def connection
    Spree::ReferralMicroserviceAdapter::Connection.instance.conn
  end
end
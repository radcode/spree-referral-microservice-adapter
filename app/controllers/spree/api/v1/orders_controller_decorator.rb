Spree::Api::V1::OrdersController.class_eval do
  before_action :report_approval_to_microservice, only: :approve

  def report_approval_to_microservice
    @order.announce_order_fulfilled
  end
end
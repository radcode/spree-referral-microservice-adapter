class Spree::Api::V1::CouponsController < Spree::Api::BaseController
  before_action :require_user_auth

  def index
    render connection.proxy_get "users/#{current_api_user.id}/coupons"
  end

  private

  def require_user_auth
    unauthorized unless Spree.user_class.exists?(current_api_user.id)
  end

  def connection
    Spree::ReferralMicroserviceAdapter::Connection.instance.conn
  end
end

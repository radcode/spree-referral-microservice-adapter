module Spree
  module Api
    module V1
      class ReferralCodesController <  Spree::Api::BaseController
        def show
          render connection.proxy_get "referral_codes/#{params[:id]}"
        end

        def owner_name
          response = connection.proxy_get "referral_codes/#{params[:id]}"
          if response[:status] == 200
            user_id = JSON(response[:inline])['id'].to_i
            user = Spree::User.find_by(id: user_id)
            render json: {name: user.firstname}.to_json
          else
            render response
          end
        end

        def connection
          Spree::ReferralMicroserviceAdapter::Connection.instance.conn
        end
      end
    end
  end
end

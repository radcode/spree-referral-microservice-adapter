require 'faraday'

module Faraday
  Connection.class_eval do
    def proxy_get(path)
      response = get path
      { inline: response.body.html_safe, layout: false, status: response.status }
    end
  end
end

Spree::LineItem.class_eval do
  after_create :report_order_to_microservice
  after_update :report_order_to_microservice
  after_destroy :report_order_to_microservice

  def report_order_to_microservice
    order.request_referral_coupon_update
  end
end

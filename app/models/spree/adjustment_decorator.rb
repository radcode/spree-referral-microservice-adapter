Spree::Adjustment.class_eval do
  def self.microservice_label
    'Referral Coupon'
  end

  def from_microservice?
    label == self.class.microservice_label
  end
end

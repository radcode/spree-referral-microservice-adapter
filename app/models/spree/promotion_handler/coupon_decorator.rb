Spree::PromotionHandler::Coupon.class_eval do
  def apply
    if order.coupon_code.present?
      if promotion.present? && promotion.actions.exists?
        handle_present_promotion
      elsif Spree::Promotion.with_coupon_code(order.coupon_code).try(:expired?)
        set_error_code :coupon_code_expired
      else
        handle_microservice_response(connection.post("orders/#{order.number}/coupons", referral_microservice_payload(order)), should_create_adjustment: true)
      end
    end
    self
  end

  def remove(coupon_code)
    promotion = order.promotions.with_coupon_code(coupon_code)

    if promotion.present?
      # Order promotion has to be destroyed before line item removing
      order.order_promotions.find_by!(promotion_id: promotion.id).destroy

      remove_promotion_adjustments(promotion)
      remove_promotion_line_items(promotion)
      order.update_with_updater!

      set_success_code :adjustments_deleted
    else
      handle_microservice_response(connection.delete("orders/#{order.number}/coupons", referral_microservice_payload(order)))
    end
    self
  end

  private

  def referral_microservice_payload(order)
    {
        user_id: order&.user&.id,
        coupon: order.coupon_code,
        order_number: order.number,
        order: order.referral_microservice_payload
    }
  end

  def handle_microservice_response(response, should_create_adjustment = false)
    response_json = JSON.parse(response.body)

    if response.success?
      if should_create_adjustment
        amount = response_json["amount"]
        order.set_referral_adjustment(amount)
      else
        order.clear_referral_adjustments
      end

      set_success_code :passed_to_microservice
    else
      error_msg = response_json["error"]
      case error_msg
      when Spree.t(:coupon_code_max_usage)
        set_error_code :coupon_code_max_usage
      when Spree.t(:already_referred)
        set_error_code :already_referred
      else
        set_error_code :coupon_code_not_found
      end
    end
  end

  def connection
    Spree::ReferralMicroserviceAdapter::Connection.instance.conn
  end
end

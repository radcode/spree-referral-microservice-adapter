# frozen_string_literal: true

Spree::user_class.class_eval do
  after_create :announce_user_create
  after_destroy :announce_user_delete
  after_update :announce_user_update

  REFERRAL_FRENZY_START_DATE = '2019-08-10'
  REFERRAL_FRENZY_FINISH_DATE = '2019-09-10'
  REFERRAL_FRENZY_THRESHOLD_PRICE = '100'
  ORDER_PAID = 'paid'

  def announce_user_create
    connection.post 'users', user_payload
  end

  def announce_user_delete
    connection.delete "users/#{id}", user_payload
  end

  def announce_user_update
    connection.put "users/#{id}", user_payload
  end

  def microservice_user_data(field_name)
    response = connection.get "users/#{id}"
    return nil if !response.success?

    hash = JSON.parse response.body
    hash[field_name]
  end

  def referral_code
    microservice_user_data('code')
  end

  def referrer_id
    microservice_user_data('referrer')
  end

  def coupons
    response = connection.get "users/#{id}/coupons"
    JSON.parse response.body
  end

  def credits
    response = connection.get "users/#{id}/credits"
    JSON.parse response.body
  end

  def refer_stats
    response = connection.get "users/#{id}/refer_stats"
    response_json = JSON.parse response.body

    parse_refer_stats response_json
  end

  def self.all_refer_stats
    response = microservice_connection.get "all_refer_stats"
    response_json = JSON.parse response.body

    response_json.transform_values(&method(:parse_refer_stats))
  end

  def self.export_to_referral_microservice
    all.each(&:announce_user_create)
  end

  private

  def user_payload
    {
      id: id,
      email: email,
      firstname: firstname,
      lastname: lastname,
      phone: phone,
      created_at: created_at,
    }
  end

  def self.microservice_connection
    Spree::ReferralMicroserviceAdapter::Connection.instance.conn
  end

  def connection
    self.class.microservice_connection
  end

  private

  alias_method :as_json_without_coupons, :serializable_hash

  def self.parse_refer_stats(raw_refer_stats)
    logs = raw_refer_stats.select do |referData|
      Spree::User.find_by(id: referData['id']).present?
    end

    logs.map do |referData|
      referee = Spree::User.find_by(id: referData['id'])
      log_entry = {
          id: referee.id,
          firstName: referee.firstname,
          createdAt: referee.created_at,
      }

      if referData['referred_at']
        log_entry['referredAt'] = Time.at(referData['referred_at'].to_i / 1000).to_datetime    # throwing away milliseconds
        unless log_entry['referredAt'] < REFERRAL_FRENZY_START_DATE || referee.orders.where(['completed_at < ?', log_entry['referredAt']]).first.present?
          qualifying_order = referee.orders.where(['payment_state = ? and item_total >= ? and delivery_at <= ?',
                                                   ORDER_PAID, REFERRAL_FRENZY_THRESHOLD_PRICE, REFERRAL_FRENZY_FINISH_DATE]).first
          if qualifying_order
            log_entry['deliveryAt'] = qualifying_order.delivery_at
          end
        end

        if referData['membership_order']
          membership_order = Spree::Order.find_by(number: referData['membership_order'])
          if membership_order&.payment_state == ORDER_PAID && membership_order&.approved_at >= log_entry['referredAt']
            log_entry['membershipPaidAt'] = membership_order.delivery_at
          end
        end
      end

      log_entry
    end
  end

  def parse_refer_stats(raw_refer_stats)
    self.class.parse_refer_stats(raw_refer_stats)
  end

  public
  def as_json_with_coupons(options = {})
    options[:methods] = (options[:methods] || []) + %i[referral_code referrer_id coupons credits refer_stats]
    as_json_without_coupons(options)
  end
end

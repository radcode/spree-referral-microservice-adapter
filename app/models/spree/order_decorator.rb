Spree::Order.class_eval do
  state_machine.before_transition to: :complete, do: :validate_coupons
  state_machine.after_transition to: %i[complete resumed], do: :announce_order_completed
  state_machine.after_transition to: :resumed, do: :announce_order_resumed
  state_machine.after_transition to: :canceled, do: :announce_order_canceled


  def clear_referral_adjustments
    adjustments.select(&:from_microservice?).each(&:destroy)
  end

  def set_referral_adjustment(money)
    amount = Spree::Money.new(money)
    if !amount.money.zero?
      adjustment = Spree::Adjustment.find_or_initialize_by(
          order: self,
          adjustable: self,
          label: Spree::Adjustment.microservice_label,
      )
      adjustment.amount = (amount.money * -1).to_s

      adjustment.save!
    else
      clear_referral_adjustments
    end
  end

  def request_referral_coupon_update
    handle_microservice_completion(
        connection.post("orders/#{number}/update", referral_microservice_payload),
        permissive: false
    )
  end

  def referral_microservice_payload
    JSON.parse Spree::Api::V1::OrdersController.new.render_anywhere('spree/api/v1/orders/show.v1.rabl', assigns: { order: self, :current_user_roles => ['admin'] })
  end

  def announce_order_fulfilled
    handle_microservice_response(connection.post("orders/#{number}/fulfilled", referral_microservice_payload))
  end

  private

  def validate_coupons
    handle_microservice_response(connection.post("orders/#{number}/confirm", referral_microservice_payload), permissive: false)
  end

  def announce_order_completed
    handle_microservice_response(connection.post("orders/#{number}/completed", referral_microservice_payload))
  end

  def announce_order_resumed
    handle_microservice_response(connection.post("orders/#{number}/resumed", referral_microservice_payload))
  end

  def announce_order_canceled
    handle_microservice_response(connection.post("orders/#{number}/cancelled", referral_microservice_payload))
  end

  def handle_microservice_completion(response, error_key: :base, error_value: :coupon_service_order_block, permissive: true)
    if response.success?
      amount = JSON.parse(response.body)["amount"]
      set_referral_adjustment(amount)
    else
      errors.add(error_key, error_value)
      errors.add(:coupon_service_response, response.body)
      false unless permissive # break callback chain
    end
  end

  def handle_microservice_response(response, error_key: :base, error_value: :coupon_service_order_block, permissive: true)
    if response.success?
      yield(response) if block_given?
    else
      errors.add(error_key, error_value)
      errors.add(:coupon_service_response, response.body)
      false unless permissive # break callback chain
    end
  end

  def connection
    Spree::ReferralMicroserviceAdapter::Connection.instance.conn
  end
end

require 'faraday'
require 'faraday_middleware/response/follow_redirects'
require 'faraday_middleware'

module Spree
  module ReferralMicroserviceAdapter
    class Connection
      include Singleton

      attr_reader :conn

      # def post
      #   handle_microservice_not_responding {super}
      # end
      #
      # def delete
      #   handle_microservice_not_responding {super}
      # end
      #
      # def put
      #   handle_microservice_not_responding {super}
      # end
      #
      # def get
      #   handle_microservice_not_responding {super}
      # end
      #
      # def handle_microservice_not_responding
      #   begin
      #     yield
      #   rescue Faraday::ConnectionFailedFaraday::ConnectionFailed
      #     throw("Could not connect to the microservice - please make sure " +
      #           "that the URI provided in the configuration (see README) " +
      #           "is pointing to a running instance of referral microservice"
      #     )
      #   end
      # end

      def initialize
        @conn = Faraday.new(url: microservice_path) do |conn|
          Spree::ReferralMicroserviceAdapter.configuration.connection_options.call(conn)
        end
      end

      private

      def microservice_path
        Spree::ReferralMicroserviceAdapter.configuration.path
      end
    end
  end
end
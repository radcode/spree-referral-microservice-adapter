class AddFieldsToSpreeUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :spree_users, :firstname, :string unless column_exists? :spree_users, :firstname
    add_column :spree_users, :lastname, :string unless column_exists? :spree_users, :lastname
    add_column :spree_users, :phone, :string unless column_exists? :spree_users, :phone
    add_column :spree_users, :birthday, :datetime unless column_exists? :spree_users, :birthday
  end
end
